# README #

Medias DiscPublish Server runs as a node server with Socket.io. 
It handles requests from medias-serverAPI to publish disc orders, pause publishing, and cancel orders.
It simply forwards these requests to the DiscPublish Agent (installed on client PC of the Epson DiscPublisher).
The Agent handles all the rest of the needed logic for actually performing the publish, pause, and cancel tasks.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact